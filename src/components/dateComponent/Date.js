import './styles.css';

export const Date = (props) => {
    let handleChange = (event) => {
        props.setFormData({...props.formData,dob: event.target.value});
    }

    return (
        <div className="dateClass">
            <input type="date" onChange={handleChange} value={props.formData.dob} required />
        </div>
    )
}