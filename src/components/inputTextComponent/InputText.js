import './styles.css';

export const InputText = (props) => {
    let handleChange = (event) => {
        props.setFormData({...props.formData,name: event.target.value});
    }
    
    return (
        <div className="nameClass">
            <input className="inputText" type="text" placeholder="Enter name" onChange={handleChange} value={props.formData.name} required />         
        </div>
    )
}