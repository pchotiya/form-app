import { RowItem } from "../RowItemComponent/RowItem"
import './styles.css';

export const UserList = (props) => {
    console.log("mpa aaya: ",props.usersDetailsMap);
    let usersDetailsMap = props.usersDetailsMap;
    return (
        <div id="table">
            <div className="tr">
                <div className="td">Name</div>
                <div className="td">Email</div>
                <div className="td">Subject</div>
                <div className="td">Level</div>
                <div className="td">Gender</div>
                <div className="td">DOB</div>
            </div>
            {
                    Object.keys(usersDetailsMap).map((id) => (
                        <RowItem key={id} userDetail={usersDetailsMap[id]} setUserMap={props.setUserMap} usersDetailsMap={usersDetailsMap} id={id} setResultDisplay={props.setResultDisplay} setFormData={props.setFormData} setIdForEdit={props.setIdForEdit} />
                    ))
            }
        </div>
    );
}