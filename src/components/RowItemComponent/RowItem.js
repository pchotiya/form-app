import './styles.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrash,faEdit } from '@fortawesome/free-solid-svg-icons'

export const RowItem = (props) => {
    let userDetail = props.userDetail;

    let handleDelete = (id) => {
        let usersDetailsMap = delete props.usersDetailsMap[id];
        props.setUserMap(usersDetailsMap);
    }

    let handleEdit = (id) => {
        props.setFormData(props.usersDetailsMap[id]);
        props.setIdForEdit(id);
        props.setResultDisplay(true);
    }

    return (
        <div className="tr">
                <div className="td">{userDetail.name}</div>
                <div className="td">{userDetail.email}</div>
                <div className="td">{userDetail.subject}</div>
                <div className="td">{userDetail.level}</div>
                <div className="td">{userDetail.gender}</div>
                <div className="td">{userDetail.dob}</div>
                <div className="td" id="icons">
                    <FontAwesomeIcon className="delete" icon={faTrash} onClick={() => {return handleDelete(props.id)}} />
                    <FontAwesomeIcon className="edit" icon={faEdit} onClick={() => {return handleEdit(props.id)}} />
                </div>
            </div>
    )
}