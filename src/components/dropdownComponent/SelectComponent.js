export const SelectComponent = (props) => {

    let handleChange = (event) => {
        props.setFormData({...props.formData,subject: event.target.value});
    }

    return (
        <div className="dropdown">
            <select onChange={handleChange} defaultValue={props.formData.subject}>    
                <option value="Science">Science</option>
                <option value="Commerce">Commerce</option>
                <option value="Arts">Arts</option>
            </select>
        </div>
    )
}