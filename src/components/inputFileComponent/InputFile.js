import './styles.css';

export const InputFile = (props) => {

    let handleChange = (event) => {
        console.log(event.target.files[0]);
        props.setFormData({...props.formData,file: event.target.files[0]});
    }
    
    // let defaultVal;
    // // console.log("file: ",props.formData.file);
    // if(props.formData.file.current === null){
    //     defaultVal = "";
    // }
    // else{
    //     defaultVal = props.formData.file;
    // }

    return (
        <div className="file">
            <input type="file" onChange={handleChange} value={props.formData.file.current} required />
        </div>
    )   
}