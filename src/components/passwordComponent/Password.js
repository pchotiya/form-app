import './styles.css';

export const Password = (props) => {

    let handlePasswordChange = (event) => {
        props.setFormData({...props.formData,password: event.target.value});
    }

    let handleConfirmPasswordChange = (event) => {
        props.setFormData({...props.formData,confirmPassword: event.target.value});
    }

    return (
        <div className="passwords">
        <div className="passwordSection">
            <input type="password" placeholder="password" onChange={handlePasswordChange} value={props.formData.password} required />
        </div>
        <div className="confirmPasswordSection">
            <input type="password" placeholder="confirm password" onChange={handleConfirmPasswordChange} value={props.formData.confirmPassword} required />
        </div>
        </div>
    )
}