import './styles.css';

export const Email = (props) => {

    let handleChange = (event) => {
        props.setFormData({...props.formData,email: event.target.value});
    }

    return (
        <div className="emailClass">
            <input type="email" placeholder="Enter email" onChange={handleChange} value={props.formData.email} required />
        </div>
    )
}