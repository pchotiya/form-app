import './App.css';
import React, { useState, useEffect } from 'react';
import { InputText } from './components/inputTextComponent/InputText';
import { InputFile } from './components/inputFileComponent/InputFile';
import { SelectComponent } from './components/dropdownComponent/SelectComponent';
import { RadioButton } from './components/radioComponent/RadioButton';
import { Range } from './components/rangeComponent/Range';
import { Date } from './components/dateComponent/Date';
import { Password } from './components/passwordComponent/Password';
import { Email } from './components/emailComponent/Email';
import { UserList } from './components/userListComponent/UserList';


function App() {
  let [id,setId] = useState(1); 
  let [isIdForEdit,setIdForEdit] = useState(0);
  let jsonFormData = {
    name: "",
    email: "",
    file: React.createRef(),
    subject: "Science",
    gender: "",
    level: 0,
    dob: "",
    password: "",
    confirmPassword: ""
  };

  
  let [usersDetailsMap,setUserMap] = useState({});
  let [resultDisplay,setResultDisplay] = useState(true);
  let [formData, setFormData] = useState(jsonFormData);

  let handleSubmit = (event) => { 
    event.preventDefault();
    if(performValidations()){
      if(parseInt(isIdForEdit) === 0){
        usersDetailsMap[id] = formData;
        setId(id+1);
      }
      else{
        usersDetailsMap[isIdForEdit] = formData;
        setIdForEdit(0);
      }
      setUserMap(usersDetailsMap);
      setResultDisplay(false);
    }
  }

  let performValidations = () => {
    if(formData.password !== formData.confirmPassword){
      alert("Password and confirm password fields are not matching");
      return false;
    }

    return true;
  }

  let handleReturnToForm = () => {
    setFormData(jsonFormData);
    setResultDisplay(true);
  }

  if(resultDisplay){
    return (
    <div>
      <button className="travelBtn" onClick={() => setResultDisplay(false)}>View List</button>
      <div className="container">
      <form className="form" onSubmit={handleSubmit}>
        <div className="inputField">
          <span>Name:</span><span className="space"></span><InputText formData={formData} setFormData={setFormData} />
        </div>
        <div className="inputField">
          <span>Email:</span><span className="space"></span><Email formData={formData} setFormData={setFormData} />
        </div>

        <div className="inputField">
          <span>Profile:</span><span className="space"></span><InputFile formData={formData} setFormData={setFormData} />
        </div>

        <div className="inputField">
          <span>Subject:</span><span className="space"></span><SelectComponent formData={formData} setFormData={setFormData} />
        </div>

        <div className="inputField">
          <span>Gender:</span><span className="space"></span><RadioButton formData={formData} setFormData={setFormData} />
        </div>

        <div className="inputField">
          <span>Level:</span><span className="space"></span><Range formData={formData} setFormData={setFormData} />
        </div>

        <div className="inputField">
          <span>DOB:</span><span className="space"></span><Date formData={formData} setFormData={setFormData} />
        </div>

        <div className="inputField">
          <span>Password:</span><span className="space"></span><Password formData={formData} setFormData={setFormData} />
        </div>

        <div className="submitBtn">
          <input className="submit" type="submit" value="Submit"/>
        </div>
      </form>
      </div>
    </div>
  );
  }
  else{
    return (
      <div className="usersList">
        <button className="travelBtn" onClick={handleReturnToForm}>Return</button>
        <UserList usersDetailsMap={usersDetailsMap} setUserMap={setUserMap} setResultDisplay={setResultDisplay} setFormData={setFormData} setIdForEdit={setIdForEdit} />
      </div>
    );
  }
}

export default App;
