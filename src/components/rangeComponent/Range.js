import './styles.css';

export const Range = (props) => {

    let handleChange = (event) => {
        props.setFormData({...props.formData,level: event.target.value});
    }

    return (
        <div className="range">
            <input type="range" id="rating" name="rating" min="0" max="10" defaultValue={props.formData.level} onChange={handleChange}/>
        </div>
    )
}