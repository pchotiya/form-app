import './styles.css';

export const RadioButton = (props) => {

    let handleChange = (event) => {
        props.setFormData({...props.formData,gender: event.target.value});
    }

    return (
        <div className="radioGroup">
            <div className="maleGender">
                <input type="radio" checked={props.formData.gender==="male"} id="male" name="gender" value="male" onChange={handleChange} required />
                <label htmlFor="male">Male</label><br />
            </div>
            <div className="femaleSection">
                <input type="radio" checked={props.formData.gender==="female"} id="female" name="gender" value="female" onChange={handleChange} required />
                <label htmlFor="female">Female</label><br />
            </div>
        </div>
    )
}